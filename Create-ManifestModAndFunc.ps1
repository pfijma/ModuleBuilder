# 
# this module creates a complete set module 
# in a subfolder with the name <ModuleName>

#make sure you select the right directory to run
#cd C:\admin\Scripts\inc

$ModuleName = 'LogIT'
$ModulePath = $ModuleName 

$moduleVersion = '1.0'
$description = 'Short Description / Functionality'
$FQDN = 'it-pro.nl'

#default manifest variables
$ManifestParms=@{
  'ModuleVersion'=$ModuleVersion 
  'Author' = 'Paul Fijma'
  'CompanyName' = 'YourCompanyName'
  'Copyright'=('{0} YourCompanyName, all rights reserved.' -f (get-date -Format yyyy))
  'Description' = $description 
  'Path'= ".\$modulename.psd1" 
  #'ProjectUri'='https://{0}/Ps/Project/{1}' -f $FQDN, $modulename
  #'ReleaseNotes' ='https://{0}/Ps/Project/{1}/ReleaseNotes'-f $modulename. $FQDN
  #'HelpInfoUri'='https://{0}/Ps/Project/{1}/HelpInfo'-f $modulename, $FQDN
  'NestedModules'= '.\{0}\{1}.psm1' -f $ModulePath, $moduleName
}

#write new module psm1
$dd =  get-date -format 'yyyyMMdd HH:mm.ss'

#default contents for psm1 
$psmMod = @'
#
# Export the module members - KUDOS to the chocolatey project for this efficent code
# creation date: yyyymmddhhmmss
#

#get the path of where the module is saved (if module is at c:\myscripts\module.psm1, then c:\myscripts\)
$mypath = (Split-Path -Parent -Path $MyInvocation.MyCommand.Definition)

#find all the ps1 files in the subfolder functions
Resolve-Path -Path $mypath\*.ps1 | ForEach-Object -Process {
    . $_.ProviderPath
}

#export as module members the functions we specify (use * for ALL functions, beware of duplicates)
Export-ModuleMember -Function *
		
#
# Define any alias and export them 
#
'@ -replace('yyyymmddhhmmss',$dd) 

#default script contents
$ps1mod = @'
#this is a function to test if module works
#
function invoke-test{
  write-host 'test invoked, module created on: yyyymmddhhmmss'
}
'@ -replace('yyyymmddhhmmss',$dd) 

#default scripts folder (here we place the scripts (*.ps1) and psm1 file)
if (!(test-path ('.\{0}\' -f $modulePath))) {
    mkdir ('.\{0}\' -f $modulePath) 
  } else {
    write-warning ('Fatal Error: folder {0} already exists!' - $ModulePath)
    BREAK
}

#export  contents to file
$psmMod | out-file ('.\{0}\{1}.psm1' -f $modulePath,$moduleName)
$ps1Mod | out-file ('.\{0}\{1}.ps1'  -f $modulePath, 'invoke-test')

New-ModuleManifest @ManifestParms

write-host ('you need to add the functions as export to the file: .\{0}\{1}.psm1' -f $ModulePath, $ModuleName)
#load the new module
import-module ('.\{0}\{1}.psm1' -f $ModulePath, $ModuleName) -verbose
#test the test-function
invoke-test 
