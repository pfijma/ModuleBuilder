# 
# create the manifest file:
# New-ModuleManifest f:\px\Q\PxDeployment.psd1 -ModuleVersion "2.0" -Author "Paul Fijma"
#

#set-location d:\deploy\1.1a\
#set-location $PSScriptRoot

#$modules = 'auxillary','cloudflare','pxdeploy','pxcmdb','kemp'
$modules = 'CMDB'

Set-location c:\admin\scripts\inc\

foreach ($module in $modules){
  write-host $module -ForegroundColor white -BackgroundColor red
  #use force to fresh load the module every time
  import-module -name .\$module -force -verbose    
  (get-module $module).ExportedCommands.Keys
}

(get-module).Name
get-module -listavailable
